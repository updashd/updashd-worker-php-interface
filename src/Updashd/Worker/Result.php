<?php
namespace Updashd\Worker;

use Updashd\Worker\Result\Metric;
use Updashd\Worker\Result\MetricCollection;

class Result {
    
    const STATUS_SUCCESS = 'SUCCESS';
    const STATUS_NOTICE = 'NOTICE';
    const STATUS_WARNING = 'WARNING';
    const STATUS_CRITICAL = 'CRITICAL';
    
    const FIELD_STATUS = 'status';
    const FIELD_DETAILS = 'details';
    const FIELD_ERROR_CODE = 'error_code';
    const FIELD_ERROR_MESSAGE = 'error_message';
    const FIELD_METRICS = 'metrics';

    private $status;
    private $details;
    private $errorMessage;
    private $errorCode;
    private $metricCollection;
    
    /**
     * If json argument is supplied, the object will populate with it's data.
     *
     * @param null|string $json
     */
    public function __construct ($json = null) {
        if ($json) {
            $this->fromJson($json);
        }
        else {
            $this->setMetricCollection(new MetricCollection());
        }
    }
    
    public function toArray () {
        $result = array(
            self::FIELD_STATUS => $this->getStatus(),
        );

        $details = $this->getDetails();
        if ($details !== null) {
            $result[self::FIELD_DETAILS] = $details;
        }

        $errorCode = $this->getErrorCode();
        if ($errorCode !== null) {
            $result[self::FIELD_ERROR_CODE] = $errorCode;
        }

        $errorMessage = $this->getErrorMessage();
        if ($errorMessage !== null) {
            $result[self::FIELD_ERROR_MESSAGE] = $errorMessage;
        }

        $metrics = $this->getMetricCollection()->toArray();
        if ($metrics !== null) {
            $result[self::FIELD_METRICS] = $metrics;
        }

        return $result;
    }
    
    /**
     * Encode the object to a json string
     * @return string
     */
    public function toJson () {
        return json_encode($this->toArray());
    }
    
    /**
     * Process the data from a json string
     * @param string $json json encoded result
     * @throws \Exception
     */
    public function fromJson ($json) {
        $result = json_decode($json, true);
        
        if ($result === FALSE) {
            throw new \Exception('Json failed to decode: ' . json_last_error_msg(), json_last_error());
        }

        $this->fromArray($result);
    }

    public function fromArray ($input) {
        $this->setStatus($this->getValue($input, self::FIELD_STATUS));
        $this->setDetails($this->getValue($input, self::FIELD_DETAILS));

        $this->setErrorCode($this->getValue($input, self::FIELD_ERROR_CODE));
        $this->setErrorMessage($this->getValue($input, self::FIELD_ERROR_MESSAGE));

        $metricsDef = $this->getValue($input, self::FIELD_METRICS);
        $metricCollection = new MetricCollection($metricsDef);
        $this->setMetricCollection($metricCollection);
    }
    
    /**
     * Get the value from the config. For more than one level deep, you can use dot notation.
     * @param $json
     * @param $path string Dot notation path
     * @return mixed
     */
    private function getValue ($json, $path) {
        // Get reference to entire config array
        $loc = &$json;
        
        foreach (explode('.', $path) as $step) {
            // Set reference to the inner step
            $loc = &$loc[$step];
        }
        
        return $loc;
    }

    /**
     * @return mixed
     */
    public function getStatus () {
        return $this->status;
    }
    
    /**
     * @param mixed $status
     */
    public function setStatus ($status) {
        $this->status = $status;
    }
    
    /**
     * @return string
     */
    public function getDetails () {
        return $this->details;
    }
    
    /**
     * @param string $details
     */
    public function setDetails ($details) {
        $this->details = $details;
    }
    
    /**
     * @return mixed
     */
    public function getErrorMessage () {
        return $this->errorMessage;
    }
    
    /**
     * @param mixed $errorMessage
     */
    public function setErrorMessage ($errorMessage) {
        $this->errorMessage = $errorMessage;
    }
    
    /**
     * @return mixed
     */
    public function getErrorCode () {
        return $this->errorCode;
    }
    
    /**
     * @param mixed $errorCode
     */
    public function setErrorCode ($errorCode) {
        $this->errorCode = $errorCode;
    }

    public function addMetricString (string $key, string $name = '', string $unit = '') {
        $this->addMetricType($key, Metric::TYPE_STRING, $name, $unit);
    }

    public function addMetricFloat (string $key, string $name = '', string $unit = '') {
        $this->addMetricType($key, Metric::TYPE_FLOAT, $name, $unit);
    }

    public function addMetricInt (string $key, string $name = '', string $unit = '') {
        $this->addMetricType($key, Metric::TYPE_INT, $name, $unit);
    }

    public function addMetricText (string $key, string $name = '', string $unit = '') {
        $this->addMetricType($key, Metric::TYPE_TEXT, $name, $unit);
    }

    public function addMetricType (string $key, string $type, string $name = '', string $unit = '') {
        $metric = new Metric($type, $name, $unit);
        $this->addMetric($key, $metric);
    }

    /**
     * Add a metric to the result metric collection.
     * @param string $key
     * @param Metric $metric
     */
    public function addMetric ($key, Metric $metric) {
        $this->getMetricCollection()->addMetric($key, $metric);
    }

    /**
     * Remove a metric from the result metric collection.
     * @param string $key
     */
    public function removeMetric ($key) {
        $this->getMetricCollection()->removeMetric($key);
    }

    /**
     * Get a metric from the result metric collection
     * @param string $key
     * @param bool $throwException
     * @return Metric
     */
    public function getMetric ($key, $throwException = true) {
        return $this->getMetricCollection()->getMetric($key, $throwException);
    }

    /**
     * @param string $key
     * @param string|int|float|null $value
     */
    public function setMetricValue ($key, $value) {
        $this->getMetricCollection()->setMetricValue($key, $value);
    }

    /**
     * @param string $key
     */
    public function getMetricValue ($key) {
        $this->getMetricCollection()->getMetricValue($key);
    }

    /**
     * @return MetricCollection
     */
    public function getMetricCollection () {
        return $this->metricCollection;
    }

    /**
     * @param MetricCollection $metricCollection
     */
    public function setMetricCollection (MetricCollection $metricCollection) {
        $this->metricCollection = $metricCollection;
    }
}