<?php
namespace Updashd\Worker;

use Updashd\Configlib\Config;

interface WorkerInterface {
    
    /**
     * Get the readable name of the service
     */
    public static function getReadableName ();
    
    /**
     * Get the name of the service (this should match in the database)
     * @return string
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public static function getServiceName ();
    
    /**
     * Create and return a Config object for this service
     * @return Config
     */
    public static function createConfig ();

    /**
     * Create and return a Result object for this service
     * The result of this function must include every field.
     * It is used to register fields in the database.
     *
     * @return Result
     */
    public static function createResult ();
    
    /**
     * Create a worker instance for the given service
     * @param Config $config the Config object used for configuration
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public function __construct (Config $config);
    
    /**
     * Run the given test
     * @throws \Updashd\Worker\Exception\WorkerRuntimeException
     * @return Result the results of the test
     */
    public function run ();
}