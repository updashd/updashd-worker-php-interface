<?php
namespace Updashd\Worker\Result;

class MetricCollection {
    const MAX_KEY_LENGTH = 30;

    private $metrics = array();

    /**
     * MetricCollection constructor.
     * @param null|array $array constructor will call fromArray if this parameter is provided.
     * @throws \Exception
     */
    public function __construct ($array = null) {
        if ($array) {
            $this->fromArray($array);
        }
    }

    /**
     * Return count of number of elements.
     * @return int
     */
    public function count () {
        return count($this->metrics);
    }

    /**
     * @param string $key Max length of key is 30 characters
     * @param Metric $metric
     * @throws \Exception
     */
    public function addMetric($key, Metric $metric) {
        $key = $this->filterKey($key);

        $this->metrics[$key] = $metric;
    }

    /**
     * Remove a metric from the collection
     * @param string $key
     * @throws \Exception
     */
    public function removeMetric($key) {
        $key = $this->filterKey($key);

        unset($this->metrics[$key]);
    }

    /**
     * Set the value of a metric.
     * @param string $key
     * @param string|int|float|null $value
     * @throws \Exception
     */
    public function setMetricValue($key, $value) {
        $key = $this->filterKey($key);
        $metric = $this->getMetric($key);
        $metric->setValue($value);
    }

    /**
     * Get the value of a certain metric in the collection.
     * @param string $key
     * @param int $precision what precision to round floats to
     * @return string|int|float|null
     * @throws \Exception
     */
    public function getMetricValue ($key, $precision = 3) {
        $key = $this->filterKey($key);
        return $this->getMetric($key)->getValue($precision);
    }

    /**
     * Get the metric object
     * @param string $key
     * @param bool $throwException If set to true, null will be returned instead of an exception thrown
     * @return Metric|null
     * @throws \Exception
     */
    public function getMetric ($key, $throwException = true) {
        $key = $this->filterKey($key);

        if (! array_key_exists($key, $this->metrics)) {
            if ($throwException) {
                throw new \Exception('Cannot get metric for non-existent key: ' . $key);
            }
            else {
                return null;
            }
        }

        return $this->metrics[$key];
    }

    /**
     * Returns an array of metrics with the key being the key to be used for name.
     * @return Metric[]
     */
    public function getMetrics () {
        return $this->metrics;
    }

    /**
     * Set an array of metrics. Key is the key name for the metric. Value is a Metric object.
     * @param Metric[] $metrics
     */
    public function setMetrics ($metrics) {
        $this->metrics = $metrics;
    }

    /**
     * @param bool $includeNull
     * @return array
     */
    public function toArray ($includeNull = false) {
        $output = [];

        foreach ($this->getMetrics() as $key => $metric) {
            if ($includeNull || $metric->getValue() !== null) {
                $output[$key] = $metric->toArray();
            }
        }

        return $output;
    }

    /**
     * @param array $input
     * @throws \Exception
     */
    public function fromArray ($input) {
        foreach ($input as $key => $metricDef) {
            $metric = new Metric();
            $metric->fromArray($metricDef);

            $this->addMetric($key, $metric);
        }
    }

    /**
     * @param string $key
     * @return string
     * @throws \Exception
     */
    protected function filterKey ($key) {
        if (strlen($key) > self::MAX_KEY_LENGTH) {
            throw new \Exception('Key length is too long: "' . $key . '". Max length: ' . self::MAX_KEY_LENGTH);
        }

        return $key;
    }
}