<?php
namespace Updashd\Worker\Result;

class Metric {
    const TYPE_INT = 'INT';
    const TYPE_FLOAT = 'FLOAT';
    const TYPE_STRING = 'STR';
    const TYPE_TEXT = 'TXT';

    const STRING_MAX_LENGTH = 200;
    const TEXT_MAX_LENGTH = 16777215;

    protected $type;
    protected $value;
    protected $unit;
    protected $name;

    /**
     * Create a metric object
     * @param string $type One of Metric::TYPE_INT, Metric::TYPE_FLOAT, or Metric::TYPE_STRING
     * @param string $name
     * @param string $unit The unit type (eg. seconds, count, etc)
     * @param string|int|float|null $value
     */
    public function __construct (string $type = self::TYPE_INT, string $name = '', string $unit = '', $value = null) {
        $this->setType($type);
        $this->setName($name);
        $this->setUnit($unit);
        $this->setValue($value);
    }

    /**
     * @return mixed
     */
    public function getType () {
        return $this->type;
    }

    /**
     * @param string $type One of Metric::TYPE_INT, Metric::TYPE_FLOAT, or Metric::TYPE_STRING
     * @throws \Exception
     */
    public function setType (string $type) {
        switch ($type) {
            case self::TYPE_FLOAT:
                break;
            case self::TYPE_INT:
                break;
            case self::TYPE_STRING:
                break;
            case self::TYPE_TEXT:
                break;
            default:
                throw new \Exception('Unknown type: ' . $type);
                break;
        }

        $this->type = $type;
    }

    /**
     * @param int $precision precision for float values
     * @return mixed
     */
    public function getValue ($precision = null) {
        if ($precision != null && $this->getType() == self::TYPE_FLOAT) {
            return round($this->value, $precision);
        }

        return $this->value;
    }

    /**
     * @param mixed $value
     * @throws \Exception
     */
    public function setValue ($value) {
        $type = $this->getType();

        if ($value !== null) {
            switch ($type) {
                case self::TYPE_FLOAT:
                    if (! is_numeric($value)) {
                        throw new \Exception('Invalid type for value: ' . $value . '. Should be type: ' . $type);
                    }
                    break;
                case self::TYPE_INT:
                    if (! is_numeric($value)) {
                        throw new \Exception('Invalid type for value: ' . $value . '. Should be type: ' . $type);
                    }
                    $value = round($value);
                    break;
                case self::TYPE_STRING:
                    if (! is_string($value)) {
                        throw new \Exception('Invalid type for value: ' . $value . '. Should be type: ' . $type);
                    }

                    if (strlen($value) > self::STRING_MAX_LENGTH) {
                        throw new \Exception('String too long for type: len=' . strlen($value) . '. max=' . self::STRING_MAX_LENGTH);
                    }

                    break;
                case self::TYPE_TEXT:
                    if (! is_string($value)) {
                        throw new \Exception('Invalid type for value: ' . $value . '. Should be type: ' . $type);
                    }

                    if (strlen($value) > self::TEXT_MAX_LENGTH) {
                        throw new \Exception('String too long for type: len=' . strlen($value) . '. max=' . self::TEXT_MAX_LENGTH);
                    }

                    break;
                default:
                    throw new \Exception('Unknown type: ' . $type);
                    break;
            }
        }

        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getUnit () {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit (string $unit) {
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function getName () {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName (string $name) {
        $this->name = $name;
    }

    public function toArray () {
        return [
            'type' => $this->getType(),
            'unit' => $this->getUnit(),
            'value' => $this->getValue(),
            'name' => $this->getName(),
        ];
    }

    public function fromArray ($input) {
        $this->setType($input['type']);
        $this->setUnit($input['unit']);
        $this->setValue($input['value']);
        $this->setName($input['name']);
    }
}