<?php

use Updashd\Configlib\Config;
use Updashd\Worker\Result;

require __DIR__ . '/../vendor/autoload.php';

class HelloWorldWorker implements \Updashd\Worker\WorkerInterface {
    const METRIC_FIELD_RESPONSE = 'response';

    private $config;
    
    public static function getReadableName () {
        return 'Hello World';
    }
    
    public static function getServiceName () {
        return 'hello_world';
    }

    public function run () {
        $result = self::createResult();
        $result->setStatus(Result::STATUS_SUCCESS);
        $result->setMetricValue(self::METRIC_FIELD_RESPONSE, 'Hello!');
        return $result;
    }
    
    /**
     * Create a worker instance for the given service
     * @param Config $config the Config object used for configuration
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public function __construct (Config $config) {
        $this->setConfig($config);
    }
    
    /**
     * @return Config
     */
    public function getConfig () {
        return $this->config;
    }
    
    /**
     * @param Config $config
     */
    public function setConfig (Config $config) {
        $this->config = $config;
    }

    /**
     * Create and return a Config object for this service
     * @return Config
     */
    public static function createConfig () {
        $config = new Config();
        $config->addFieldText('greeting', 'Greeting', 'Hello', true);
        $config->addFieldText('subject', 'Subject', 'World', true);

        return $config;
    }

    /**
     * Create and return a Result object for this service.
     * The result of this function must include every field.
     * It is used to register fields in the database.
     *
     * @return Result
     */
    public static function createResult () {
        $result = new Result();
        $result->addMetricString(self::METRIC_FIELD_RESPONSE, 'Response');
        return $result;
    }
}

$config = HelloWorldWorker::createConfig();
$config->setValue('subject', 'Updashd');

$worker = new HelloWorldWorker($config);

$result = $worker->run();

print_r($result->toArray());